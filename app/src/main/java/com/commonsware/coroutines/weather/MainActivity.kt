/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Kotlin Coroutines_

  https://commonsware.com/Coroutines
*/

package com.commonsware.coroutines.weather

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.observe
import androidx.recyclerview.widget.*
import com.commonsware.coroutines.weather.databinding.ActivityMainBinding
import com.commonsware.coroutines.weather.databinding.RowBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {
  private val motor: MainMotor by viewModel()

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    val binding = ActivityMainBinding.inflate(layoutInflater)

    setContentView(binding.root)

    val adapter = ObservationAdapter()

    binding.observations.layoutManager = LinearLayoutManager(this)
    binding.observations.adapter = adapter
    binding.observations.addItemDecoration(
      DividerItemDecoration(
        this,
        DividerItemDecoration.VERTICAL
      )
    )

    motor.states.observe(this) { state ->
      when (state) {
        MainViewState.Loading -> binding.progress.visibility = View.VISIBLE
        is MainViewState.Content -> {
          binding.progress.visibility = View.GONE
          adapter.submitList(state.observations)
        }
        is MainViewState.Error -> {
          binding.progress.visibility = View.GONE
          Toast.makeText(
            this@MainActivity, state.throwable.localizedMessage,
            Toast.LENGTH_LONG
          ).show()
          Log.e("Weather", "Exception loading data", state.throwable)
        }
      }
    }

    motor.refresh()
  }

  override fun onCreateOptionsMenu(menu: Menu): Boolean {
    menuInflater.inflate(R.menu.actions, menu)

    return super.onCreateOptionsMenu(menu)
  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    when (item.itemId) {
      R.id.refresh -> { motor.refresh(); return true }
      R.id.clear -> { motor.clear(); return true }
    }

    return super.onOptionsItemSelected(item)
  }

  inner class ObservationAdapter :
    ListAdapter<RowState, RowHolder>(RowStateDiffer) {
    override fun onCreateViewHolder(
      parent: ViewGroup,
      viewType: Int
    ) = RowHolder(RowBinding.inflate(layoutInflater, parent, false))

    override fun onBindViewHolder(holder: RowHolder, position: Int) {
      holder.bind(getItem(position))
    }
  }

  class RowHolder(private val binding: RowBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(state: RowState) {
      binding.state = state
      binding.executePendingBindings()
    }
  }

  object RowStateDiffer : DiffUtil.ItemCallback<RowState>() {
    override fun areItemsTheSame(
      oldItem: RowState,
      newItem: RowState
    ): Boolean {
      return oldItem === newItem
    }

    override fun areContentsTheSame(
      oldItem: RowState,
      newItem: RowState
    ): Boolean {
      return oldItem == newItem
    }
  }
}
